package badshah.chandrapal.dice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    // Created with the help of the article : https://medium.com/@ssaurel/learn-to-create-a-roll-dice-game-on-android-a612167361cf

    public static final Random RANDOM = new Random();
    private Button rollDices;
    private ImageView imageView1, imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rollDices = (Button) findViewById(R.id.rollDices);
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);

        rollDices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value1 = randomDiceValue();
                int value2 = randomDiceValue();

                int res1 = getResources().getIdentifier("dice_" + value1, "drawable", "badshah.chandrapal.dice");
                int res2 = getResources().getIdentifier("dice_" + value2, "drawable", "badshah.chandrapal.dice");

                imageView1.setImageResource(res1);
                imageView2.setImageResource(res2);

                if(value1 + value2 == 12) {
                    Toast.makeText(getApplicationContext(), "Mission Accomplished !", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Try harder !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public static int randomDiceValue() {
        return RANDOM.nextInt(6) + 1;
    }
}
